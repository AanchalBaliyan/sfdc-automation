package SF;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class ContactsTCTest {


	public static void main(String[] args) throws InterruptedException {
		
		createnewcontact();
		createnewview();
		recentlycreated();
		mycontacts();
		viewcontacts();
		errormsgcreateview();
		cancelnewview();
		saveandnewcreatecontact();	

	}

	
	@Test
	public static void createnewcontact() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\aanchal\\Drivers\\chromedriver.exe");
		WebDriver driver;
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		Thread.sleep(2000);
		
		WebElement userName = driver.findElement(By.xpath("//input[@id='username']"));
		userName .sendKeys("aarohi.gupta.june28-eywo@force.com");
		WebElement password = driver.findElement(By.xpath("//input[@id='password']"));
		password.sendKeys("windows8");
		WebElement loginbutton = driver.findElement(By.xpath("//input[@id='Login']"));
		loginbutton.click();
		Thread.sleep(1000);
		WebElement closewindow = driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closewindow.click();
		Thread.sleep(1000);
		WebElement contacts = driver.findElement(By.xpath("//a[contains(text(),'Contacts')]"));
		contacts.click();
		Thread.sleep(1000);
		WebElement newacc = driver.findElement(By.xpath("//input[@name='new']"));
		newacc.click();
		Thread.sleep(1000);
		WebElement lastnamecontact = driver.findElement(By.xpath("//input[@id='name_lastcon2']"));
		lastnamecontact .sendKeys("Sue2");
		Thread.sleep(1000);
		WebElement accountnamecontacts = driver.findElement(By.xpath("//input[@id='con4']"));
		accountnamecontacts.clear();
		accountnamecontacts.sendKeys("River Mountain Outfitters");
		Thread.sleep(1000);
		WebElement savecontact = driver.findElement(By.xpath("//td[@id='topButtonRow']//input[@name='save']"));
		savecontact.click();
		
	}

	@Test 
    public static void createnewview() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\aanchal\\Drivers\\chromedriver.exe");
		WebDriver driver;
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		Thread.sleep(2000);
		
		WebElement userName = driver.findElement(By.xpath("//input[@id='username']"));
		userName .sendKeys("aarohi.gupta.june28-eywo@force.com");
		WebElement password = driver.findElement(By.xpath("//input[@id='password']"));
		password.sendKeys("windows8");
		WebElement loginbutton = driver.findElement(By.xpath("//input[@id='Login']"));
		loginbutton.click();
		Thread.sleep(1000);
		WebElement closewindow = driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closewindow.click();
		Thread.sleep(1000);
		WebElement contacts = driver.findElement(By.xpath("//a[contains(text(),'Contacts')]"));
		contacts.click();
		Thread.sleep(1000);
		WebElement createview = driver.findElement(By.xpath("//a[contains(text(),'Create New View')]"));
		createview.click();
		Thread.sleep(1000);
		WebElement viewname = driver.findElement(By.xpath("//input[@id='fname']"));
		viewname .sendKeys("Baliyan");
		Thread.sleep(1000);
		WebElement viewuniname = driver.findElement(By.xpath("//input[@id='devname']"));
		viewuniname.sendKeys("aanchal1234"
				+ "");
		Thread.sleep(1000);
		WebElement saveview = driver.findElement(By.xpath("//div[@class='pbBottomButtons']//input[@name='save']"));
		saveview.click();
		
}
	
	@Test
    public static void recentlycreated() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\aanchal\\Drivers\\chromedriver.exe");
		WebDriver driver;
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		Thread.sleep(2000);
		
		WebElement userName = driver.findElement(By.xpath("//input[@id='username']"));
		userName .sendKeys("aarohi.gupta.june28-eywo@force.com");
		WebElement password = driver.findElement(By.xpath("//input[@id='password']"));
		password.sendKeys("windows8");
		WebElement loginbutton = driver.findElement(By.xpath("//input[@id='Login']"));
		loginbutton.click();
		Thread.sleep(1000);
		WebElement closewindow = driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closewindow.click();
		Thread.sleep(1000);
		WebElement contacts = driver.findElement(By.xpath("//a[contains(text(),'Contacts')]"));
		contacts.click();
		Thread.sleep(1000);
		WebElement viewrecentcontacts = driver.findElement(By.xpath("//select[@id='hotlist_mode']"));
		viewrecentcontacts.click();
		Thread.sleep(1000);
		WebElement recentlycreateddd = driver.findElement(By.name("Recently Created)"));
		recentlycreateddd.click();
		
    }
	
	@Test
    public static void mycontacts() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\aanchal\\Drivers\\chromedriver.exe");
		WebDriver driver;
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		Thread.sleep(2000);
		
		WebElement userName = driver.findElement(By.xpath("//input[@id='username']"));
		userName .sendKeys("aarohi.gupta.june28-eywo@force.com");
		WebElement password = driver.findElement(By.xpath("//input[@id='password']"));
		password.sendKeys("windows8");
		WebElement loginbutton = driver.findElement(By.xpath("//input[@id='Login']"));
		loginbutton.click();
		Thread.sleep(1000);
		WebElement closewindow = driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closewindow.click();
		Thread.sleep(1000);
		WebElement contacts = driver.findElement(By.xpath("//a[contains(text(),'Contacts')]"));
		contacts.click();
		Thread.sleep(1000);
		WebElement viewdd = driver.findElement(By.xpath("//select[@id='fcf']"));
		viewdd.click();
		Thread.sleep(1000);
		WebElement mycontactsdd = driver.findElement(By.name("My Contacts"));
		mycontactsdd.click();
		Thread.sleep(1000);
		WebElement gomycontacts = driver.findElement(By.xpath("//input[@name='go']"));
		gomycontacts.click();    
    
}
	
	@Test
    public static void viewcontacts() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\aanchal\\Drivers\\chromedriver.exe");
		WebDriver driver;
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		Thread.sleep(2000);
		
		WebElement userName = driver.findElement(By.xpath("//input[@id='username']"));
		userName .sendKeys("aarohi.gupta.june28-eywo@force.com");
		WebElement password = driver.findElement(By.xpath("//input[@id='password']"));
		password.sendKeys("windows8");
		WebElement loginbutton = driver.findElement(By.xpath("//input[@id='Login']"));
		loginbutton.click();
		Thread.sleep(1000);
		WebElement closewindow = driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closewindow.click();
		Thread.sleep(1000);
		WebElement contacts = driver.findElement(By.xpath("//a[contains(text(),'Contacts')]"));
		contacts.click();
		Thread.sleep(1000);
		WebElement viewrecentcontacts = driver.findElement(By.xpath("//select[@id='hotlist_mode']"));
		viewrecentcontacts.click();
		Thread.sleep(1000);
		WebElement recentlycreateddd = driver.findElement(By.name("Recently Created)"));
		recentlycreateddd.click();
		WebElement anycontacts = driver.findElement(By.xpath("//a[contains(text(),'Escober (Sample), Jack')]"));
		anycontacts.click();
		
    }
	
	@Test
 public static void errormsgcreateview() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\aanchal\\Drivers\\chromedriver.exe");
		WebDriver driver;
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		Thread.sleep(2000);
		
		WebElement userName = driver.findElement(By.xpath("//input[@id='username']"));
		userName .sendKeys("aarohi.gupta.june28-eywo@force.com");
		WebElement password = driver.findElement(By.xpath("//input[@id='password']"));
		password.sendKeys("windows8");
		WebElement loginbutton = driver.findElement(By.xpath("//input[@id='Login']"));
		loginbutton.click();
		Thread.sleep(1000);
		WebElement closewindow = driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closewindow.click();
		Thread.sleep(1000);
		WebElement contacts = driver.findElement(By.xpath("//a[contains(text(),'Contacts')]"));
		contacts.click();
		Thread.sleep(1000);
		WebElement createview = driver.findElement(By.xpath("//a[contains(text(),'Create New View')]"));
		createview.click();
		Thread.sleep(1000);
		WebElement viewname = driver.findElement(By.xpath("//input[@id='fname']"));
		viewname .sendKeys("");
		Thread.sleep(1000);
		WebElement viewuniname = driver.findElement(By.xpath("//input[@id='devname']"));
		viewuniname.sendKeys("EFGH");
		Thread.sleep(1000);
		WebElement saveview = driver.findElement(By.xpath("//div[@class='pbBottomButtons']//input[@name='save']"));
		saveview.click();
 }
	
	@Test 
 public static void cancelnewview() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\aanchal\\Drivers\\chromedriver.exe");
		WebDriver driver;
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		Thread.sleep(2000);
		
		WebElement userName = driver.findElement(By.xpath("//input[@id='username']"));
		userName .sendKeys("aarohi.gupta.june28-eywo@force.com");
		WebElement password = driver.findElement(By.xpath("//input[@id='password']"));
		password.sendKeys("windows8");
		WebElement loginbutton = driver.findElement(By.xpath("//input[@id='Login']"));
		loginbutton.click();
		Thread.sleep(1000);
		WebElement closewindow = driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closewindow.click();
		Thread.sleep(1000);
		WebElement contacts = driver.findElement(By.xpath("//a[contains(text(),'Contacts')]"));
		contacts.click();
		Thread.sleep(1000);
		WebElement createview = driver.findElement(By.xpath("//a[contains(text(),'Create New View')]"));
		createview.click();
		Thread.sleep(1000);
		WebElement viewname = driver.findElement(By.xpath("//input[@id='fname']"));
		viewname .sendKeys("ABCD");
		Thread.sleep(1000);
		WebElement viewuniname = driver.findElement(By.xpath("//input[@id='devname']"));
		viewuniname.sendKeys("EFGH");
		Thread.sleep(1000);
		WebElement cancelview = driver.findElement(By.xpath("//div[@class='pbHeader']//input[@name='cancel']"));
		cancelview.click();	
 }
	
	@Test
 public static void saveandnewcreatecontact() throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "C:\\aanchal\\Drivers\\chromedriver.exe");
		WebDriver driver;
		driver=new ChromeDriver();
		driver.get("https://login.salesforce.com/");
		Thread.sleep(2000);
		
		WebElement userName = driver.findElement(By.xpath("//input[@id='username']"));
		userName .sendKeys("aarohi.gupta.june28-eywo@force.com");
		WebElement password = driver.findElement(By.xpath("//input[@id='password']"));
		password.sendKeys("windows8");
		WebElement loginbutton = driver.findElement(By.xpath("//input[@id='Login']"));
		loginbutton.click();
		Thread.sleep(1000);
		WebElement closewindow = driver.findElement(By.xpath("//a[@id='tryLexDialogX']"));
		closewindow.click();
		Thread.sleep(1000);
		WebElement contacts = driver.findElement(By.xpath("//a[contains(text(),'Contacts')]"));
		contacts.click();
		Thread.sleep(1000);
		WebElement newacc = driver.findElement(By.xpath("//input[@name='new']"));
		newacc.click();
		Thread.sleep(1000);
		WebElement lastnamecontact = driver.findElement(By.xpath("//input[@id='name_lastcon2']"));
		lastnamecontact .sendKeys("Kumber");
		Thread.sleep(1000);
		WebElement accountnamecontacts = driver.findElement(By.xpath("//input[@id='con4']"));
		accountnamecontacts.clear();
		accountnamecontacts.sendKeys("Trail Expert Co.");
		Thread.sleep(1000);
		WebElement saveandnewcontact = driver.findElement(By.xpath("//td[@id='topButtonRow']//input[@name='save']"));
		saveandnewcontact.click();
		
	}



}
